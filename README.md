# CatOS

Fully functional operation system, manage everything.

## Minium Requirements

* CPU
* Screen Driver
* GPU
* RAM X 4

## Features

* Auto craft when storage was low on stock
* Minecraft AE mod crafting experience
* Fully functional console

### Installation

1. Copy all the files and folder to ```%LocalAppData%\FactoryGame\Saved\SaveGames\Computers\<YOUR DRIVE UUID>```
2. Copy ```configure-templete.lua``` to the root drive and rename it to ```configure.lua```
3. Edit ```configure.lua``` with your favorite editor(or just notepad)
4. Paste ```boot.lua``` code to in-game editor(EEPROM), enter your root drive UUID and run it.
5. If no error occur, there's the CatOS :D

## Milestones

* Support Locomotives
* UI Framework
* Editor
