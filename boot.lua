-------------------------------
-- Bootloader of the CatOS
-- Put this to your EPPROM
-- REQUIREMENTS:
--   * CPU
--   * RAM * 4
--   * EEPROM
-------------------------------
-- Global variables
-- Enter your disk uuid
root_uuid = ""
fs = filesystem

-- Main program

-- Mount root disk
if root_uuid == "" then
    computer.panic("No boot disk specified.")
end

local res = fs.initFileSystem("/dev")
for i = 1, 10 do
    if res == true then break end
    event.pull(1)
    computer.skip()
    res = fs.initFileSystem("/dev")
end

if res == false then
    computer.panic("Cannot mount /")
end

fs.mount("/dev/"..root_uuid, "/")

-- Load kernel
fs.doFile("/kernel/init.lua")

