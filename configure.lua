-- Configuration of CatOS
SysConfig = {
    -- Environment variables
    env_vars = {
        OS_VER = "0.2",
        HOME = "/", -- Pls make sure it exist!
        CWD = "", -- dont set this variable! set by FISH
        PATH = {"/bin", "/lib"}, -- it can be a list of paths
    },

    -- Config screens
    screens = {
        console_screen = {
            -- console screen, it must be a ScreenDriver
            size = {100, 30}
        },
    },
    -- Set UI Color
    ui_colors = {
        font = {
            normal = {1, 1, 1, 1}, -- white
            info = {0.8, 0.8, 0.8, 1}, -- grey
            warning = {1, 0.956, 0.239, 1}, -- yellow
            error = {1, 0, 0, 1}, -- red
            highlight = {0, 0, 0, 1}
        },
        background = {0, 0, 0, 0},
        border = {1, 0.584, 0.078, 1},
    },

    -- Shell environment
    max_history = 10,
    shell_sym = "><>",
    shell_sym_color = {1, 0.584, 0.078, 1}, -- orange
    -- Commands will execute on start
    init_cmds = {
        -- Print some message for fun
        "echo ><> FicsIt Shell <><",
        "echo Star System: Akycha",
        "echo Planet: MASSAGE-2(A-B)b",
        "echo Salary: 10 Beryl Nut/mo",
        "echo Perks: None",
        "echo Welcome Pioneer!",
    },

    -- Mount Points: Will automacticly mount on boot
    mountpoints = {
        -- mountpoint = Disk_UUID
        -- DO NOT ADD ROOT!
        {"home", "7A36A3D64428974C334E89B0C37DEE0D"},
    },

    device_mgr = {

    },

    ---------------------------------
    --  OS Module Configurations  --
    ---------------------------------

    -- Pastebin
    -- pastebin = {
    --     dev_key = "",

    -- },
    
    -- LAN Networking
    lan_networking = {
        refresh_interval = 0.2, -- in seconds
    },

    -- Storage Monitor
    storage_monitor = {
        enable = false,
        screen_nick = "storage_screen",
        update_interval = 1,
    },

    -- Auto Crafting
    auto_crafting = {
        refresh_interval = 1, -- in seconds
        auto_refill = false, -- Automaticly craft when low on stock
        auto_refill_thoushold = 100, -- Item amount to trigger auto filling
    },

    -- Power
    power_monitor = {
        refresh_interval = 1, -- in seconds
        notice_fused = false, -- Notify player if fused
        reconnect_when_fused = false, -- Auto reconnect after fused
    },

}