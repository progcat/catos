-- Ficsit Shell
-- Commands builtin
-- * cd - Change directory
-- * set - Set environment
-- * unset -- Unset environment
-- * cp -- Copy file(s) or folder(s)
-- * mv -- Move file(s) or folder(s)
-- * rm -- Remove file(s) or folder(s)
-- * mkdir -- Create folder(s)
-- * ls -- List directory
-- * echo -- Print text
-- * clear -- Clear the text buffer
-- * reboot -- Reboot the computer
-- * shutdown -- Shutdown the computer
-- * time -- display game time
-- * clear -- clear screen
import("lib/strutils.lua")

FISH = {
    cwd = SysConfig.env_vars.HOME,
    history = {},
    history_idx = 1,
    line_buff = {}, -- store all the lines
    cmd_buff = {},
    cmd_idx = 1,
    col = 0,
    line = 0,
    max_col = 0,
    max_line = 0,
    screen_size = {0, 0},
    caplocked = false,
    first_start = true,
    screen = nil,
    running_program = false,
    program_pid = 0,
}

function FISH.init(screen)
    FISH.screen = screen
    gpu:bindScreen(screen)
    gpu:setSize(SysConfig.screens.console_screen.size[1], SysConfig.screens.console_screen.size[2])
    -- check screen size
    FISH.screen_size = {gpu:getSize()}
    FISH.max_col = FISH.screen_size[1] - 1
    FISH.max_line = FISH.screen_size[2] - 1

    FISH.clearScreen()
    event.ignoreAll()
    -- Spawn update thread
    local pid = TaskMgr:spawn(FISH.update)
    TaskMgr:start(pid)
end

function FISH.setFontColor(color, bg_color)
    if bg_color then
        gpu:setBackground(bg_color[1], bg_color[2], bg_color[3], bg_color[4])
    end
    gpu:setForeground(color[1], color[2], color[3], color[4])
end

function FISH.getScreen()
    return FISH.screen
end

function FISH.clearScreen()
    FISH.setFontColor(SysConfig.ui_colors.font.normal, SysConfig.ui_colors.background)
    gpu:fill(0, 0, FISH.screen_size[1], FISH.screen_size[2], " ")
end

function FISH.clearBuffer()
    FISH.col = 0
    FISH.line = 0
    FISH.line_buff = {}
end

function FISH.print(msg, color, bg_color)
    msg = tostring(msg)
    -- do a text wrap
    msg = str.textWrap(msg, FISH.col, FISH.max_col)
    local lines = str.split(msg, '\n')
    for idx, text in ipairs(lines) do
        local section = {
            col = FISH.col,
            line = FISH.line,
            msg = text,
            color = SysConfig.ui_colors.font.normal,
            bg_color = SysConfig.ui_colors.background
        }
        if color then
            section.color = color
            FISH.setFontColor(color, bg_color)
        end
        if bg_color then
            section.bg_color = bg_color
        end
        table.insert(FISH.line_buff, section)
        if #lines == 1 then
            FISH.col = FISH.col + string.len(msg)
        elseif idx <= #lines then
            FISH.line = FISH.line + 1
            FISH.col = 0
        end
    end
    -- handle newline at the end
    if string.sub(msg, #msg, #msg) == '\n' then
        FISH.line = FISH.line + 1
        FISH.col = 0
    end
    -- check scrolling
    if FISH.line > FISH.max_line then
        -- scroll up one line
        for idx, section in ipairs(FISH.line_buff) do
            -- update line
                section.line = section.line - 1
        end
        -- remove line that exceeded border
        local continue = true
        while continue do
            computer.skip() -- without this, out of time exception will occurr
            continue = false
            for i = 1, #FISH.line_buff do
                if FISH.line_buff[i].line < 0 then
                    table.remove(FISH.line_buff, i)
                    continue = true
                    break
                end
            end
        end
        FISH.line = FISH.max_line
        FISH.col = 0
    end
end

function FISH.println(msg, color, bg_color)
    FISH.print(tostring(msg) .. '\n', color, bg_color)
end

-- print out errors
function FISH.error(msg)
    FISH.println(msg, SysConfig.ui_colors.font.error)
end

function FISH.warning(msg)
    FISH.println(msg, SysConfig.ui_colors.font.warning)
end

function FISH.runProgram(cmd, args)
    -- if it is not a path, automaticly treat as its come from the PATH
    local path = str.parsePath(FISH.cwd, cmd)
    if string.find(cmd, '/') == nil then
        -- search $PATH
        for _, v in ipairs(SysConfig.env_vars.PATH) do
            local p = str.parsePath(v, cmd)
            
            if fs.isFile(p) == true then
                path = p
                break
            end
        end
    end
    -- if it is start from a slash it is an absolute path
    if fs.isFile(path) == true then
        local prog = fs.loadFile(path)
        if type(prog) ~= "function" then
            FISH.error(tostring(prog))
            FISH.error("Could not load program.")
            return false
        end
        -- spawn a single run thread
        FISH.program_pid = TaskMgr:spawn(prog(), true, table.unpack(args))
        TaskMgr:setErrorHandler(FISH.program_pid, debug.traceback)
        TaskMgr:setName(FISH.program_pid, path)
        TaskMgr:start(FISH.program_pid)
        FISH.running_program = true
        return true
    else
        FISH.error("Command not found!")
        return false
    end
end

function FISH.exit()
    if FISH.running_program then
        TaskMgr:kill(FISH.program_pid)
        coroutine.yield()
    end
end

function FISH.executeCmd(cmd_buff)
    local args = str.split(cmd_buff, ' ') -- separate with space
    local builtin_cmd_lookup = {
        clear = FISH.clearBuffer,
        reboot = computer.reset,
        shutdown = computer.stop,
    }
    if builtin_cmd_lookup[args[1]] then
        builtin_cmd_lookup[args[1]](args)
    else
        local cmd = args[1]
        table.remove(args, 1)
        FISH.runProgram(cmd, args)
    end
end

function FISH.printPrompt()
    FISH.col = 0
    FISH.print(FISH.cwd .. ' ')
    FISH.print(SysConfig.shell_sym .. ' ', SysConfig.shell_sym_color)
end

function FISH.drawCmdBuffer()
    -- when there is no letter to point
    for i = 1, #FISH.cmd_buff do
        FISH.setFontColor(SysConfig.ui_colors.font.normal, SysConfig.ui_colors.background)
        if FISH.cmd_idx == i then
            FISH.setFontColor(SysConfig.ui_colors.font.highlight, SysConfig.ui_colors.font.normal)
        end
        gpu:setText(FISH.col + i - 1, FISH.line, FISH.cmd_buff[i])
    end
    if FISH.cmd_idx > #FISH.cmd_buff then
        FISH.setFontColor(SysConfig.ui_colors.font.highlight, SysConfig.ui_colors.font.normal)
        gpu:setText(FISH.col + FISH.cmd_idx - 1, FISH.line, ' ')
    end
end

function FISH.render()
    FISH.setFontColor(SysConfig.ui_colors.font.normal, SysConfig.ui_colors.background)
    FISH.clearScreen()
    -- print buffer
    for _, section in pairs(FISH.line_buff) do
        -- clear text behind it
        FISH.setFontColor(section.color, section.bg_color)
        gpu:setText(section.col, section.line, section.msg)
    end

    FISH.drawCmdBuffer()

    gpu:flush()
end

function FISH.update()
    -- update environment variables
    SysConfig.env_vars.CWD = FISH.cwd
    gpu:bindScreen(FISH.screen)
    event.listen(gpu)
    
    FISH.render()
    if FISH.first_start then
        FISH.clearScreen()
        FISH.clearBuffer()
        -- Run init commands
        for _, cmd in pairs(SysConfig.init_cmds) do
            -- Lazy, just copy from below
            FISH.executeCmd(cmd)
            -- kill thread
            if TaskMgr:isDead(FISH.program_pid) then

                local err_msg = TaskMgr:getError(FISH.program_pid)
                if err_msg then
                    FISH.error(err_msg)
                end
                FISH.running_program = false
            end
        end
        if #SysConfig.init_cmds == 0 then
            FISH.printPrompt()
        end
        FISH.first_start = not FISH.first_start
    end

    -- update running program
    if FISH.running_program then
        if TaskMgr:isDead(FISH.program_pid) then

            local err_msg = TaskMgr:getError(FISH.program_pid)
            if err_msg then
                FISH.error(err_msg)
            end
            TaskMgr:kill(FISH.program_pid)
            FISH.running_program = false
            -- Ugly fix for scolling bug
            if FISH.line >= FISH.max_line then FISH.println("") end
            FISH.printPrompt()
        end
    end

    e, s, char, code, btn = event.pull(0.1)
    if e == nil then
        event.ignoreAll()
        return
    end
    if e == "OnKeyDown" then
        local key = str.getKeyStroke(char, code, btn, FISH.caplocked)
        if key then
            if key == "CAPS" then
                FISH.caplocked = not FISH.caplocked
            elseif key == "ESC" then
                FISH.exit()
            elseif key == "RET" then
                -- push command to the buffer
                local cmd = str.trim(str.join(FISH.cmd_buff))
                FISH.println(cmd)
                if #cmd > 0 then
                    FISH.col = 0
                    -- execute command
                    FISH.executeCmd(cmd)

                    table.insert(FISH.history, FISH.cmd_buff)
                    if #FISH.history > 10 then
                        table.remove(FISH.history, 1)
                    end
                end
                FISH.col = 0
                FISH.history_idx = #FISH.history + 1
                FISH.cmd_idx = 1
                FISH.cmd_buff = {}
                if not FISH.running_program then
                    FISH.printPrompt()
                end

            elseif key == "BACK" then
                -- delete letter on left
                if FISH.cmd_idx > 1 then
                    table.remove(FISH.cmd_buff, FISH.cmd_idx - 1)
                    FISH.cmd_idx = FISH.cmd_idx - 1
                end
            elseif key == "UP" then
                if FISH.history_idx > 1 then
                    FISH.history_idx = FISH.history_idx - 1
                end
                if #FISH.history > 1 then
                    FISH.cmd_buff = FISH.history[FISH.history_idx]
                    FISH.cmd_idx = #FISH.cmd_buff + 1
                end

            elseif key == "DOWN" then
                if FISH.history_idx < #FISH.history then
                    FISH.history_idx = FISH.history_idx + 1
                end
                if #FISH.history > 1 then
                    FISH.cmd_buff = FISH.history[FISH.history_idx]
                    FISH.cmd_idx = #FISH.cmd_buff + 1
                end

            elseif key == "LEFT" then
                -- move cursor to left
                if FISH.cmd_idx > 1 then
                    FISH.cmd_idx = FISH.cmd_idx - 1
                end

            elseif key == "RIGHT" then
                -- move cursor to right
                if FISH.cmd_idx <= #FISH.cmd_buff then
                    FISH.cmd_idx = FISH.cmd_idx + 1
                end

            elseif key == "DEL" then
                -- delete letter on the right
                if FISH.cmd_idx <= #FISH.cmd_buff then
                    table.remove(FISH.cmd_buff, FISH.cmd_idx)
                end

            elseif string.len(key) == 1 then
                -- letter, digits, symbols goes here
                -- add to the command buffer
                if FISH.cmd_idx < FISH.max_col then
                    table.insert(FISH.cmd_buff, FISH.cmd_idx, key)
                    FISH.cmd_idx = FISH.cmd_idx + 1
                end
            end
        end
    end
    -- event.clear()
end
