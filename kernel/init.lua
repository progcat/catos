-- The entry point of the kernel

-- For System-wide require
package = {
    loaded = {}
}

-- Helper function
function import(path)
    if type(path) == "string" then
        fs.doFile(path)
    elseif type(path) == "table" then
        for _, lib in ipairs(path) do
            fs.doFile(lib)
        end
    end
end

function require(path)
    return fs.doFile(path)()
    -- search $PATH
    -- search cwd
end

print("CatOS Initializing...")

-- Load components for starting the kernel
import({
    "configure.lua",
    "kernel/taskmgr.lua",
    "kernel/storage.lua",
    })

-- Initialize console screen
gpu = computer.getGPUs()[1]
if gpu == nil then
    computer.panic("GPU not found!")
end
-- find any available ScreenDriver (mostly the first one)
local console_screen = nil
for _, screen in ipairs(computer.getScreens()) do
    if tostring(screen) == "ScreenDriver" then
        console_screen = screen
        break
    end
end
if console_screen == nil then computer.panic("You must have a ScreenDriver installed!") end

-- Initialize mountpoints
print(#SysConfig.mountpoints..' mountpoints found.')
if #SysConfig.mountpoints > 0 then
    -- Mount all drives
    for _, value in pairs(SysConfig.mountpoints) do
        local drive_path = string.format("/dev/%s", value[2])
        local mountpoint = string.format("/%s", value[1])
        print(string.format("Mounting %s to %s", drive_path, mountpoint))
        fs.mount(drive_path, mountpoint)
    end
end

-- Initialize modules
StorageMonitor:init()

-- Start the terminal
import("fish/fish.lua")
FISH.init(console_screen)

-- Hello world
TaskMgr:run()
computer.panic("Task manager dead!")