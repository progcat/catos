StorageMonitor = {}

function StorageMonitor.init()
    if SysConfig.storage_monitor.enable == false then return end
    if SysConfig.storage_monitor.screen_nick == "" then
        computer.panic("You must specify a nickname for Storage Monitor's screen!")
    end
    local devs = component.findComponent(SysConfig.storage_monitor.screen_nick)
    if #devs < 1 then computer.panic("No screen found!") end
    StorageMonitor.screen = component.proxy(devs[1])
    -- Spawn probe thread
    StorageMonitor.probe_pid = TaskMgr:spawnTimed(SysConfig.storage_monitor.update_interval, StorageMonitor.probeUpdate)
    -- Spawn ui thread 
    StorageMonitor.interface_pid = TaskMgr:spawn(StorageMonitor.interfaceUpdate)
    -- start threads
end

function StorageMonitor.probeUpdate()

end

function StorageMonitor.interfaceUpdate()

end