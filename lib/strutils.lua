-- Utils functions for string
fs = filesystem
str = {}

function str.split(src, sep)
    local arr= {}
    for tok in src:gmatch("[^"..sep.."]+") do
        table.insert(arr, tok)
    end
    return arr
end

-- remove all spaces at the end of string
function str.trim(src)
    local txt = ""
    local ended = false
    for char in src:reverse():gmatch('.') do
        if not ended and char ~= ' ' then ended = true end
        if ended then
            txt = char..txt
        end
    end
    return txt
end

function str.join(arr, sep)
    if sep then
        return table.concat(arr, sep)
    else
        return table.concat(arr)
    end
end

function str.startWith(text, c)
    return string.sub(text, 1, #c) == c
end

-- WIKI is your friend:
-- https://en.wikipedia.org/wiki/Line_wrap_and_word_wrap
function str.textWrap(text, current_col, max_col)
    if current_col + string.len(text) <= max_col then return text end
    local words = str.split(text, ' ')
    local space_left = max_col - current_col
    for i, word in ipairs(words) do -- split with spaces
        computer.skip()
        local word_len = string.len(word)
        if word_len > space_left then
            words[i] = '\n'..words[i]
            space_left = max_col - word_len
        else
            space_left = space_left - word_len
        end
    end
    return str.join(words, ' ')
end

function str.getKeyStroke(char, code, btn, caplocked)
    local shift = (btn == 8)
    -- ASCII range: 65 - 90
    local lowercase = {
        'a', 'b', 'c', 'd', 'e',
        'f', 'g', 'h', 'i', 'j',
        'k', 'l', 'm', 'n', 'o',
        'p', 'q', 'r', 's', 't',
        'u', 'v', 'w', 'x', 'y',
        'z'
    }
    local uppercase = {
        'A', 'B', 'C', 'D', 'E',
        'F', 'G', 'H', 'I', 'J',
        'K', 'L', 'M', 'N', 'O',
        'P', 'Q', 'R', 'S', 'T',
        'U', 'V', 'W', 'X', 'Y', 'Z'
    }
    -- ASCII range: 44 - 57
    local digits = {',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
    local shift_digits = {'<', '_', '>', '?', ')', '!', '@', '#', '$', '%', '^', '&', '('}
    
    local specials = {"PGUP", "PGDN", "END", "HOME", "LEFT", "UP", "RIGHT", "DOWN"}

    if char >= 44 and char <= 57 then
        if shift then return shift_digits[char-44+1] else return digits[char-44+1] end
    elseif char >= 65 and char <= 90 then
        -- do a xor for uppercase
        if not(shift == caplocked) then return uppercase[char-65+1] else return lowercase[char-65+1] end
    elseif code >= 33 and code <= 40 then
        return specials[code-33+1]
    end

    -- Remaining special keys
    if code == 46 then return "DEL" end
    if code == 45 then return "INS" end
    if code == 20 then return "CAPS" end

    if char == 32 then return ' ' end
    if char == 9 then return '\t' end
    if char == 13 then return "RET" end
    if char == 8 then return "BACK" end
    if char == 27 then return "ESC" end

    if shift then
        if char == 61 then return "+" end
        if char == 91 then return "{" end
        if char == 93 then return "}" end
        if char == 59 then return ":" end
        if char == 39 then return "\"" end
        if char == 92 then return "|" end
        if char == 96 then return "~" end
    else
        if char == 61 then return "=" end
        if char == 91 then return "[" end
        if char == 93 then return "]" end
        if char == 59 then return ";" end
        if char == 39 then return "'" end
        if char == 92 then return "\\" end
        if char == 96 then return "`" end
    end
    return nil
end

function str.parsePath(init_path, path)
    if path == "/" then return path end
    local full_path = init_path..'/'..path..'/'
    str.trimPath(full_path)
    local dirs = string.gmatch(full_path, '[%w%_.-]*/')
    local final_path = {}
    for dir in dirs do
        -- previous directory
        if dir == '../' then
            if #final_path >= 1 then 
                table.remove(final_path)
            else
                -- invalid path
                error("Invalid path!")
                return ""
            end
        elseif dir ~= './' then
            table.insert(final_path, dir)
        end
    end
    return str.trimPath(str.join(final_path))
end

function str.trimPath(path)
    -- remove all '/./'
    local n = 0
    while true do
        path, n = string.gsub(path, '/%./', '/')
        if n == 0 then break end
    end
    -- remove all '//'
    while true do
        path, n = string.gsub(path, '//', '/')
        if n == 0 then break end
    end
    if string.len(path) > 1 and string.sub(path, string.len(path)) == '/' then
        path = string.sub(path, 1, string.len(path)-1)
    end
    return path
end